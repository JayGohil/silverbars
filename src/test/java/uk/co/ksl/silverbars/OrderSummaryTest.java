package uk.co.ksl.silverbars;

import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class OrderSummaryTest {
    @Test
    public void WHEN_order_is_added_and_compataible_THAT_merge_adds_prices() {
        OrderSummary unit = OrderSummary.builder()
                .orderDirection(OrderDirection.BUY)
                .quantity(1)
                .pricePerKg(314.1)
                .build();

        Order order = Order.builder()
                .orderDirection(OrderDirection.BUY)
                .quantity(2)
                .pricePerKg(271.8)
                .build();

        Optional<OrderSummary> mergedSummary = unit.mergeNewOrder(order);
        assertFalse(mergedSummary.isPresent());
    }

    @Test
    public void WHEN_order_is_cancelled_and_compataible_THAT_merge_adds_prices() {
        OrderSummary unit = OrderSummary.builder()
                .orderDirection(OrderDirection.BUY)
                .quantity(3)
                .pricePerKg(314.1)
                .build();

        Order order = Order.builder()
                .orderDirection(OrderDirection.BUY)
                .quantity(2)
                .pricePerKg(314.1)
                .build();

        Optional<OrderSummary> mergedSummary = unit.mergeCancelledOrder(order);

        assertTrue(mergedSummary.isPresent());
        assertThat(mergedSummary.get().getQuantity(), is(1.0));
    }


    @Test
    public void WHEN_order_is_not_compatible_THAT_merge_fails() {
        OrderSummary unit = OrderSummary.builder()
                .orderDirection(OrderDirection.BUY)
                .pricePerKg(314.1)
                .quantity(1)
                .build();

        Order order = Order.builder()
                .orderDirection(OrderDirection.BUY)
                .pricePerKg(314.1)
                .quantity(2)
                .build();

        Optional<OrderSummary> mergedSummary = unit.mergeNewOrder(order);

        assertTrue(mergedSummary.isPresent());
        assertThat(mergedSummary.get().getQuantity(), is(3.0));
    }
}