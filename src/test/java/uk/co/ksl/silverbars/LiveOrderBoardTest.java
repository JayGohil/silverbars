package uk.co.ksl.silverbars;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class LiveOrderBoardTest {
    @Autowired
    private LiveOrderBoard unit;

    @Autowired
    private OrderService orderService;

    @Test
    public void GIVEN_buy_and_sell_orders_THAT_they_are_sorted_correctly_on_the_live_order_board() {
        Order sellOrder1 = Order.builder()
                .orderDirection(OrderDirection.SELL)
                .quantity(3.5)
                .pricePerKg(306.0)
                .userId("fred")
                .build();

        Order sellOrder2 = Order.builder()
                .orderDirection(OrderDirection.SELL)
                .quantity(1.2)
                .pricePerKg(310.0)
                .userId("fred")
                .build();

        Order sellOrder3 = Order.builder()
                .orderDirection(OrderDirection.SELL)
                .quantity(1.5)
                .pricePerKg(307.0)
                .userId("fred")
                .build();

        Order sellOrder4 = Order.builder()
                .orderDirection(OrderDirection.SELL)
                .quantity(2.0)
                .pricePerKg(306.0)
                .userId("fred")
                .build();

        Order buyOrder1 = sellOrder1.toBuilder().orderDirection(OrderDirection.BUY).build();
        Order buyOrder2 = sellOrder2.toBuilder().orderDirection(OrderDirection.BUY).build();
        Order buyOrder3 = sellOrder3.toBuilder().orderDirection(OrderDirection.BUY).build();
        Order buyOrder4 = sellOrder4.toBuilder().orderDirection(OrderDirection.BUY).build();

        orderService.addOrder(sellOrder1);
        orderService.addOrder(buyOrder1);
        orderService.addOrder(sellOrder2);
        orderService.addOrder(buyOrder2);
        orderService.addOrder(sellOrder3);
        orderService.addOrder(buyOrder3);
        orderService.addOrder(sellOrder4);
        orderService.addOrder(buyOrder4);

        List<OrderSummary> liveOrderBoard = unit.liveOrderBoard();
        assertThat(liveOrderBoard, Matchers.hasSize(6));
        assertThat(liveOrderBoard.get(0), Matchers.is(OrderSummary.builder().pricePerKg(306.0).quantity(5.5).orderDirection(OrderDirection.SELL).build()));
        assertThat(liveOrderBoard.get(1),
                Matchers.is(OrderSummary.builder().pricePerKg(307.0).quantity(1.5).orderDirection(OrderDirection.SELL).build()));
        assertThat(liveOrderBoard.get(2),
                Matchers.is(OrderSummary.builder().pricePerKg(310.0).quantity(1.2).orderDirection(OrderDirection.SELL).build()));
        assertThat(liveOrderBoard.get(3),
                Matchers.is(OrderSummary.builder().pricePerKg(310.0).quantity(1.2).orderDirection(OrderDirection.BUY).build()));
        assertThat(liveOrderBoard.get(4),
                Matchers.is(OrderSummary.builder().pricePerKg(307.0).quantity(1.5).orderDirection(OrderDirection.BUY).build()));
        assertThat(liveOrderBoard.get(5),
                Matchers.is(OrderSummary.builder().pricePerKg(306.0).quantity(5.5).orderDirection(OrderDirection.BUY).build()));
    }

}