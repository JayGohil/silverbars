package uk.co.ksl.silverbars;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ConcurrentMap;

import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class OrderServiceTest {
    @Autowired
    private OrderService unit;

    @Autowired
    private ConcurrentMap<Double, OrderSummary> sellOrderSummariesByPrice;

    @Autowired
    private ConcurrentMap<Double, OrderSummary> buyOrderSummariesByPrice;

    @Test
    public void WHEN_orders_are_added_and_cancelled_THAT_entries_are_present_in_the_summary_maps() {
        Order sellOrder1 = Order.builder()
                .orderDirection(OrderDirection.SELL)
                .quantity(3.5)
                .pricePerKg(306.0)
                .userId("fred")
                .build();

        Order sellOrder2 = Order.builder()
                .orderDirection(OrderDirection.SELL)
                .quantity(1.2)
                .pricePerKg(310.0)
                .userId("fred")
                .build();



        Order buyOrder1 = sellOrder1.toBuilder().orderDirection(OrderDirection.BUY).build();
        Order buyOrder2 = sellOrder2.toBuilder().orderDirection(OrderDirection.BUY).build();

        unit.addOrder(sellOrder1);
        unit.addOrder(buyOrder1);
        unit.addOrder(sellOrder2);
        unit.addOrder(buyOrder2);

        assertThat(buyOrderSummariesByPrice.size(), is(2));
        assertThat(sellOrderSummariesByPrice.size(), is(2));
        assertThat(buyOrderSummariesByPrice, hasKey(306.0));
        assertThat(buyOrderSummariesByPrice, hasKey(310.0));
        assertThat(sellOrderSummariesByPrice, hasKey(306.0));
        assertThat(sellOrderSummariesByPrice, hasKey(310.0));

        // Now cancel an order
        // Demonstrate that when cancelling an order where the quantity becomes 0, then the order summary entry is removed...
        unit.cancelOrder(buyOrder1);
        assertThat(buyOrderSummariesByPrice.size(), is(1));
        assertThat(buyOrderSummariesByPrice, hasKey(310.0));
    }
}