package uk.co.ksl.silverbars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SilverBarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SilverBarsApplication.class, args);
	}

}
