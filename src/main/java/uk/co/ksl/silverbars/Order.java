package uk.co.ksl.silverbars;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class Order {
    private String userId;
    private double quantity;
    private double pricePerKg;
    private OrderDirection orderDirection;
}
