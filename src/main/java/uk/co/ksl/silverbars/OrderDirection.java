package uk.co.ksl.silverbars;

public enum OrderDirection
{
    BUY,
    SELL
}
