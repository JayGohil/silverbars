package uk.co.ksl.silverbars;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.ConcurrentMap;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OrderService {
    private final ConcurrentMap<Double, OrderSummary> sellOrderSummariesByPrice;
    private final ConcurrentMap<Double, OrderSummary> buyOrderSummariesByPrice;

    /** Uses compare and set operations on the hash map to remove the need for locking */
    public void addOrder(final Order order) {
        ConcurrentMap<Double, OrderSummary> orderSummariesByPrice = getSummaryMap(order);
        OrderSummary summary = OrderSummary.builder()
                .orderDirection(order.getOrderDirection())
                .pricePerKg(order.getPricePerKg())
                .quantity(order.getQuantity())
                .build();

        boolean updateSucceeded = false;
        while (!updateSucceeded) {
            OrderSummary currentSummary = orderSummariesByPrice.putIfAbsent(summary.getPricePerKg(), summary);
            if (currentSummary == null) {
                updateSucceeded = true;
                continue;
            }
            final Optional<OrderSummary> newSummary = currentSummary.mergeNewOrder(order);

            if (newSummary.isPresent()) {
                updateSucceeded = orderSummariesByPrice.replace(summary.getPricePerKg(), currentSummary, newSummary.get());
            }
        }
    }

    /** Uses compare and set operations on the hash map to remove the need for locking */
    public void cancelOrder(final Order order) {
        ConcurrentMap<Double, OrderSummary> orderSummariesByPrice = getSummaryMap(order);

        boolean updateSucceeded = false;
        while (!updateSucceeded) {
            OrderSummary currentSummary = orderSummariesByPrice.get(order.getPricePerKg());
            if (currentSummary != null) {
                Optional<OrderSummary> mergedSummary = currentSummary.mergeCancelledOrder(order);
                if (mergedSummary.isPresent()) {
                    if (mergedSummary.get().getQuantity()==0.0) {
                        // Remove this summary as the quantity is zero and not worth recording....
                        updateSucceeded = orderSummariesByPrice.remove(order.getPricePerKg(), currentSummary);
                    }
                    else {
                        updateSucceeded = orderSummariesByPrice.replace(order.getPricePerKg(), currentSummary, mergedSummary.get());
                    }
                }
            }
            else {
                throw new RuntimeException(String.format("unable to cancel order=%s", order));
            }
        }
    }

    private ConcurrentMap<Double, OrderSummary> getSummaryMap(Order order) {
        return (order.getOrderDirection()== OrderDirection.BUY) ? buyOrderSummariesByPrice : sellOrderSummariesByPrice;
    }
}
