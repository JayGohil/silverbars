package uk.co.ksl.silverbars;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LiveOrderBoard {
    private final ConcurrentMap<Double, OrderSummary> sellOrderSummariesByPrice;
    private final ConcurrentMap<Double, OrderSummary> buyOrderSummariesByPrice;


    /** Probably should have mechanism to generate the live order board from the incoming orders in real time rather than
     *  generate the presentation list on demand. Next time ;-)
     */
    public List<OrderSummary> liveOrderBoard() {
        List<OrderSummary> summaries = sellOrderSummariesByPrice.values().stream()
                .sorted(Comparator.comparingDouble(OrderSummary::getPricePerKg))
                .collect(Collectors.toList());

        summaries.addAll(buyOrderSummariesByPrice.values().stream()
                .sorted(Comparator.comparingDouble(OrderSummary::getPricePerKg).reversed())
                .collect(Collectors.toList()));

        return summaries;
    }
}
