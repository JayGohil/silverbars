package uk.co.ksl.silverbars;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

import java.util.Optional;

@Value
@Builder(toBuilder = true)
@EqualsAndHashCode(exclude = "mergeCount")
@ToString(exclude = "mergeCount")
public class OrderSummary {
    // Used as a counteduring a merge operation, helpful to determine when a different thread has snuck in and updated this
    // value object in the map...
    private final int mergeCount;

    private final OrderDirection orderDirection;
    private final double pricePerKg;
    private final double quantity;

    /** @return Optional.empty() if the order is not not compatible with the order summary record */
    public Optional<OrderSummary> mergeNewOrder(Order order) {
        if (!canMerge(order)) return Optional.empty();

        return Optional.of(this.toBuilder()
                .quantity(quantity+order.getQuantity())
                .mergeCount(mergeCount+1)
                .build());
    }

    /** @return Optional.empty() if the order is not not compatible with the order summary record */
    public Optional<OrderSummary> mergeCancelledOrder(Order order) {
        if (!canMerge(order)) return Optional.empty();

        return Optional.of(this.toBuilder()
                .quantity(quantity-order.getQuantity())
                .mergeCount(mergeCount+1)
                .build());
    }


    private boolean canMerge(Order order) {
        if (        order.getOrderDirection() == orderDirection
                &&  order.getPricePerKg() == pricePerKg) {
            return true;
        }
        return false;
    }
}
