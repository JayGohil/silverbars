package uk.co.ksl.silverbars;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Configuration
public class SilverBarsConfiguration {
    @Bean
    public ConcurrentMap<Double, OrderSummary> sellOrderSummariesByPrice() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    public ConcurrentMap<Double, OrderSummary> buyOrderSummariesByPrice() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    public ConcurrentMap<Integer, Order> orders() {
        return new ConcurrentHashMap<>();
    }
}
